using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Dev2.sacip
{
    public class cursor : MonoBehaviour
    {
        private SpriteRenderer rended;
        public Sprite Normal;
        public Sprite Click;

        // Start is called before the first frame update
        void Start()
        {
            Cursor.visible = false;
            rended = GetComponent<SpriteRenderer>();
        }
        void Update()
        {
            Vector2 cursorPo = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = cursorPo;

            if (Input.GetMouseButtonDown(0))
            {
                rended.sprite = Click;
            } else if(Input.GetMouseButtonUp(0))
            {
                rended.sprite = Normal;
            }
        }
    }
}
