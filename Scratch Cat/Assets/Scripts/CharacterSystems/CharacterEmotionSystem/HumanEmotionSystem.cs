using UnityEngine;

namespace Jomjam
{
    public class HumanEmotionSystem : CharacterEmotionSystem
    {
        protected override void Start()
        {
            base.Start();
        }

        public override void OnAddEmotion()
        {
            base.OnAddEmotion();
            
            float happy = GM.humanHappy - m_speedInsEmotion;
            
            if(GM.humanHappy > 0)
                GM.humanHappy = happy;
            else if (GM.humanHappy <= 0)
                GM.humanHappy = 0;
        }

        protected override void OnChangeEmotion()
        {
            base.OnChangeEmotion();

            if (GM.humanHappy < 100)
                GM.humanHappy += Time.deltaTime * GM.humanSpeed[GM.humanState.GetHashCode()];
            else if (GM.humanHappy >= 100)
                GM.humanHappy = 100;
        }
    }
}