using System;
using UnityEngine;

namespace Jomjam
{
    public class CharacterEmotionSystem : MonoBehaviour
    {
        [SerializeField] protected float m_speedInsEmotion;
        [SerializeField] protected float m_buttonPosY = 20;
        protected GameManager GM;
        
        protected virtual void Start()
        {
            GM = GameManager.Instance;
        }

        protected virtual void Update()
        {
            OnChangeEmotion();
        }

        /*
        private void OnGUI()
        {
            if(GUI.Button(new Rect(20,m_buttonPosY,150, 50), "Add Emotion"))
                OnAddEmotion();
        }*/

        public virtual void OnAddEmotion()
        {
            
        }

        protected virtual void OnChangeEmotion()
        {
            
        }
    }
}