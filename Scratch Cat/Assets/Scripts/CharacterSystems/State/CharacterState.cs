using System;
using System.Collections.Generic;
using UnityEngine;
using Util.Enum;

namespace Jomjam
{
    public class CharacterState : MonoBehaviour
    {
        [SerializeField] protected SpriteRenderer m_character;
        [SerializeField] protected List<Sprite> m_emotion;
        protected CharState _state;
        protected GameManager GM;

        protected virtual void OnEnable()
        {
            if(m_character == null)
                m_character = GetComponent<SpriteRenderer>();
        }

        protected virtual void Start()
        {
            GM = GameManager.Instance;
            _state = CharState.Normal;
            m_character.sprite = m_emotion[0];
        }

        protected virtual void Update()
        {
            switch (_state)
            {
                case CharState.Normal:
                    print($"State Normal == {_state}");
                    m_character.sprite = m_emotion[_state.GetHashCode()];
                    break;
                case CharState.Happy:
                    print($"State Happy == {_state}");
                    m_character.sprite = m_emotion[_state.GetHashCode()];
                    break;
                case CharState.Stressed:
                    print($"State Stressed == {_state}");
                    m_character.sprite = m_emotion[_state.GetHashCode()];
                    break;
            }
        }

        public virtual void OnChangeHappyValue(float value, List<float> points)
        {
            if (value <= points[0])
                _state = CharState.Normal;
            else if (value <= points[1])
                _state = CharState.Happy;
            else if (value <= points[2])
                _state = CharState.Stressed;
        }
    }
}