using System;
using UnityEngine;
using Util.Enum;

namespace Jomjam
{
    public class HumanWorkingSystem : MonoBehaviour
    {
        [SerializeField] private float m_speedOffset = 0.5f;
        private GameManager GM;

        private void Start()
        {
            GM = GameManager.Instance;
        }

        private void Update()
        {
            OnWorking();
        }

        private void OnWorking()
        {
            float work = 0;

            if (GM.humanState != CharState.Happy)
                work = GM.workProgrss + Time.deltaTime * (GM.workSpeed * Mathf.Abs((GM.humanSpeed[GM.humanState.GetHashCode()] - GM.humanSpeed[0]) / 10 * m_speedOffset - 1));
            else
            {
                work = GM.workProgrss + Time.deltaTime * (GM.workSpeed * Mathf.Abs(GM.humanSpeed[GM.humanState.GetHashCode()] / 10 * (m_speedOffset * 0.75f) - 1));
                
                print($"fyytdtydc = {work}");
            }

            if (work < 100)
                GM.workProgrss = work;
            else if (work >= 100)
                GM.workProgrss = 100;
        }
    }
}